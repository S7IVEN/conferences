<?php
	include("./services/router.php");
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- <meta http-equiv="X-UA-Compatible" content="ie=edge" /> -->
		<title>Andres</title>
		<!-- Bootstrap styles -->
		<link
			rel="stylesheet"
			href="../vendor/components/font-awesome/css/all.min.css"
		/>
		<link
			rel="stylesheet"
			href="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css"
		/>
		<link rel="stylesheet" href="../src/css/manage.css" />
		<link rel="stylesheet" href="../src/css/landing.css" />
		<!-- Material Icons -->
		<link
			href="https://fonts.googleapis.com/icon?family=Material+Icons"
			rel="stylesheet"
		/>
		<!-- font familys -->
		<link
			href="https://fonts.googleapis.com/css?family=Combo|Comfortaa|Coming+Soon&display=swap"
			rel="stylesheet"
		/>
		<link
			href="https://fonts.googleapis.com/css?family=Montserrat|PT+Sans|Ubuntu&display=swap"
			rel="stylesheet"
		/>
		<link
			href="https://fonts.googleapis.com/css?family=Mansalva&display=swap"
			rel="stylesheet"
		/>
		<link
			rel="stylesheet"
			id="wp-editor-font-css"
			href="https://fonts.googleapis.com/css?family=Noto+Serif%3A400%2C400i%2C700%2C700i&#038;ver=5.2.3"
			type="text/css"
			media="all"
		/>
	</head>

	<body id="cnt" class="bg-white">
		<section>
			<nav class="navbar navbar-light" style="background-color: #f5fff8">
				<a class="navbar-brand" style="color: #28a745; font-family: 'Comfortaa', 'sans-serif';">ADMINISTRADOR</a>
				<a href="./services/logout.php" class="btn btn-outline-success my-2 my-sm-0">Salir</a>
			</nav>
		</section>
		<section>
			<div class="container-fluid mt-4 row justify-content-center">
				<div class="col-12">
					<table class="table">
						<thead class="thead-light">
							<tr>
								<th scope="col">Número</th>
								<th scope="col">Nombre</th>
								<th scope="col">Apellido</th>
								<th scope="col">Telefono</th>
								<th scope="col">Correo</th>
								<th scope="col">Nombre Contacto</th>
								<th scope="col">Telefono Contacto</th>
								<th scope="col">Descripción</th>
								<th scope="col">Estado</th>
								<th scope="col">Editar</th>
								<th scope="col">Eliminar</th>
							</tr>
						</thead>
						<tbody id="render"></tbody>
					</table>
				</div>
			</div>
		</section>
	</body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
	<script
		src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"
	></script>
	<script src="../src/js/jquery-3.2.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"
	></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"
	></script>
	<script>
		function toggleState(id, element) {
			$.ajax({
				url: "./services/change_status.php",
				method: "get",
				data: { id: id },
				success: function(res) {
					let response = JSON.parse(res);
					if (response) {
						if (element.textContent == "Pendiente") {
							element.textContent = "Pagado";
							element.className = "badge badge-success";
						} else {
							element.textContent = "Pendiente";
							element.className = "badge badge-danger";
						}
					}
				}
			});
		}
		function renderFields(assistents) {
			let table = document.getElementById("render");
			let html = assistents
				.map((el, number) => {
					return `<tr><td class="text-center">${number + 1}</td><td>${el.name}</td><td>${el.lastname}</td><td>${
						el.phone
					}</td><td>${el.email}</td><td>${el.namecontact}</td><td>${
						el.phonecontact
					}</td><td>${el.description}</td><td class="text-center">${
						el.done == 0
							? '<span onclick="toggleState(' +
							  el.id +
							  ', this)" class="badge badge-danger">Pendiente</span>'
							: '<span onclick="toggleState(' +
							  el.id +
							  ', this)" class="badge badge-success">Pagado</span>'
					}</td><td class="text-center"><a href="./edit.php?id=${el.id}"><span class="fa fa-edit"><span></a></td>
					<td class="text-center"><span class="fa fa-eraser text-danger" onclick="deleteAssistent(${el.id})"><span></td></tr>`;
				})
				.join("");
			table.innerHTML = html;
		}

		function deleteAssistent(id){
			let state = confirm("Esta acción es irreversible");
			if(state) window.location.href = `./services/delete_assistent.php?id=${id}`
		}

		$.ajax({
			url: "./services/get_assistent.php",
			method: "get",
			success: function(res) {
				let response = JSON.parse(res);
				renderFields(response);
			}
		});
	</script>
</html>

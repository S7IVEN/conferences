<?php
	include("./services/router.php");
	include("config_db.php");
	$id = isset($_GET["id"]) ? $_GET["id"] : 1 ;
	$result = $conn->query("SELECT * FROM asistents WHERE id = $id");
	$result = $result->fetch_assoc();
	// echo json_encode($result); die();
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- <meta http-equiv="X-UA-Compatible" content="ie=edge" /> -->
		<title>Andres</title>
		<!-- Bootstrap styles -->
		<link
			rel="stylesheet"
			href="../vendor/components/font-awesome/css/all.min.css"
		/>
		<link
			rel="stylesheet"
			href="../vendor/twbs/bootstrap/dist/css/bootstrap.min.css"
		/>
		<link rel="stylesheet" href="../src/css/manage.css" />
		<link rel="stylesheet" href="../src/css/landing.css" />
		<!-- Material Icons -->
		<link
			href="https://fonts.googleapis.com/icon?family=Material+Icons"
			rel="stylesheet"
		/>
		<!-- font familys -->
		<link
			href="https://fonts.googleapis.com/css?family=Combo|Comfortaa|Coming+Soon&display=swap"
			rel="stylesheet"
		/>
		<link
			href="https://fonts.googleapis.com/css?family=Montserrat|PT+Sans|Ubuntu&display=swap"
			rel="stylesheet"
		/>
		<link
			href="https://fonts.googleapis.com/css?family=Mansalva&display=swap"
			rel="stylesheet"
		/>
		<link
			rel="stylesheet"
			id="wp-editor-font-css"
			href="https://fonts.googleapis.com/css?family=Noto+Serif%3A400%2C400i%2C700%2C700i&#038;ver=5.2.3"
			type="text/css"
			media="all"
		/>
	</head>

	<body id="cnt" class="bg-white">
		<section>
			<nav class="navbar navbar-light" style="background-color: #f5fff8">
				<a class="navbar-brand" style="color: #28a745; font-family: 'Comfortaa', 'sans-serif';">ADMINISTRADOR</a>
				<a href="./services/logout.php" class="btn btn-outline-success my-2 my-sm-0">Salir</a>
			</nav>
		</section>
		<section>
			<div class="container-fluid mt-4 row justify-content-center">
				<div class="col-12 col-md-6">
					<form action="./services/update_assistent.php" method="post">
						<div class="form-row justify-content-center">
							<input type="number" value="<?php echo $result["id"];?>" name="id" hidden>
							<div class="form-group col-md-6 col-12">
								<label for="inputName">Nombre</label>
								<input
									type="text"
									class="form-control"
									id="inputName"
									placeholder="Nombre"
									name="name"
									required
									value="<?php echo $result['name'];?>"
								/>
							</div>
							<div class="form-group col-md-6 col-12">
								<label for="inputLastname">Apellido</label>
								<input
									type="text"
									class="form-control"
									id="inputLastname"
									placeholder="Apellido"
									name="lastname"
									value="<?php echo $result['lastname'];?>"
								/>
							</div>
							<div class="form-group col-md-6 col-12">
								<label for="inputEmail">Correo</label>
								<input
									type="email"
									class="form-control"
									id="inputEmail"
									placeholder="Correo"
									name="email"
									value="<?php echo $result['email'];?>"
									required
								/>
							</div>
							<div class="form-group col-md-6 col-12">
								<label for="inputPhone">Teléfono</label>
								<input
									type="text"
									class="form-control"
									id="inputPhone"
									placeholder="Teléfono"
									name="phone"
									value="<?php echo $result['phone'];?>"
								/>
							</div>
							<div class="form-group col-12">
								<label for="inputDescription">Descripción</label>
								<textarea
									type="text"
									class="form-control"
									id="inputDescription"
									placeholder="Description"
									name="description"
								><?php echo $result['description'];?></textarea>
							</div>
							<div class="form-group col-md-6 col-12">
								<label for="inputNameContact">Nombre Contacto</label>
								<input
									type="text"
									class="form-control"
									id="inputNameContact"
									placeholder="Nombre del Contacto"
									name="namecontact"
									value="<?php echo $result['namecontact'];?>"
									required
								/>
							</div>
							<div class="form-group col-md-6 col-12">
								<label for="inputPhoneContact">Teléfono Contacto</label>
								<input
									type="text"
									class="form-control"
									id="inputPhoneContact"
									placeholder="Teléfono"
									name="phonecontact"
									value="<?php echo $result['phonecontact'];?>"
								/>
							</div>
						</div>
						<center>
							<button type="submit" class="mt-3 btn btn-primary">
								Guardar
							</button>
						</center>
					</form>
				</div>
			</div>
		</section>
	</body>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/parallax/3.1.0/parallax.min.js"></script>
	<script
		src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
		integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
		crossorigin="anonymous"
	></script>
	<script src="../src/js/jquery-3.2.1.min.js"></script>
	<script
		src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
		integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
		crossorigin="anonymous"
	></script>
	<script
		src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
		integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
		crossorigin="anonymous"
	></script>
</html>
